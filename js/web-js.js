$(function(){

	//Animate Opacity

	/* var objHeight = $(".animateOpacity").outerHeight();

	$(window).scroll(function () {

	    scrollTop  = $(window).scrollTop();

		if (scrollTop < objHeight) {
	    	var objOpacity = 1 - (scrollTop / objHeight);
		   	$('.animateOpacity').css("opacity", objOpacity);
		}
	}) */

	// Control hover 
	$('.pointers li').hover(function(){

		//Get Values value
		var sname= $(this).attr("data-sname");
		var fname = $(this).attr("data-fname");
		
		//Change Values
		$('.sname').text(sname),
		$('.fname').text(fname);

		//Toggle in and Out
		$(this).toggleClass('animated').toggleClass('rotate');
		if($('.brief').hasClass('fadeInUp')){
			$('.brief').removeClass('fadeInUp').addClass('fadeOutDown');
		} else {
			$('.brief').removeClass('fadeOutDown').addClass('animated fadeInUp');
		}
	})
});