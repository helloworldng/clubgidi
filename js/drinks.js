$(function(){
	var currentDrink;

	editDrink = function(currentDrink){

		$('.content').addClass("edit-mode");
		$('.card').removeClass("edit-mode");
		currentDrink.addClass("edit-mode");

		// Setting all the variables
		var panel = $('#edit-panel');
		var image = currentDrink.data( "image" );
		var price = currentDrink.data( "price" );
		var carton = currentDrink.data( "carton" );
		var know = currentDrink.data( "know" );
		var name = currentDrink.data( "name" );

		// Applying set variables to edit panel
		panel.show();
		panel.find('#image').attr("src", image );
		panel.find('#price').val(price );
		panel.find('#carton').val(carton );
		panel.find('#know').val(know );
		panel.find('#name').val(name );
		panel.find('#_name').html(name );
	};

	var exitEdit = function(){
		$('.card').removeClass("edit-mode");
		$('.edit-panel').hide();
		$('.content').removeClass("edit-mode");
	}

	$('.exit-edit').click(function(){exitEdit()});

	$('.card').click(function(){		
		currentDrink = $(this);
		editDrink($(this));
	});

	$('#filter-options').on('change', function() {
      var group = this.value;

      // Filter elements
      $('#grid').shuffle( 'shuffle', group );
    });

	$('#shuffle-search').on('keyup change', function() {
	  var val = this.value.toLowerCase();
	  $('#grid').shuffle('shuffle', function($el, shuffle) {

	    // Only search elements in the current group
	    if (shuffle.group !== 'all' && $.inArray(shuffle.group, $el.data('groups')) === -1) {
	      return false;
	    }

	    var text = $.trim( $el.find('._title').text() ).toLowerCase();
	    return text.indexOf(val) !== -1;
	  });
	});

})
